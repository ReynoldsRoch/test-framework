<%-- 
    Document   : listDept
    Created on : 9 nov. 2022, 10:34:01
    Author     : reynoldscomputer
--%>

<%
    Object[][] list = (Object[][]) request.getAttribute("listDept");
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>List Dept!</h1>
        <%out.println("test : "+request.getAttribute("dept"));%>
        
        <table border="1">
            <tr>
                <th>id</th>
                <th>nom</th>
            </tr>
            
            <%for (int i = 0; i < list.length; i++) { %>
            
            <tr>
                <%for (int j = 0; j < list[i].length; j++) { %>
                    
                <td><%out.println(list[i][j]);%></td>
                        
                <%    }
                %>
            </tr>
                    
            <%   }
            %>
            
            
        </table>

    </body>
</html>
