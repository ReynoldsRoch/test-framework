<%-- 
    Document   : listPerson
    Created on : 26 oct. 2022, 14:22:45
    Author     : reynoldscomputer
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>LIST PERSON!</h1>
        
        <%
            Object[][] select = (Object[][]) request.getAttribute("listPerson");
        %>
        
        <table border="1">
            <tr>
                <th>id</th>
                <th>nom</th>
                <th>prenom</th>
                <th>age</th>
            </tr>
            
            
            <%for (int i = 0; i < select.length; i++) { %>
            <tr>

                <%for (int j = 0; j < select[i].length; j++) { %>

                    <td><%out.println(select[i][j]);%></td>

                <%   }
                %>
            </tr>      
            <%    }
            %>
 
        </table>

    </body>
</html>
