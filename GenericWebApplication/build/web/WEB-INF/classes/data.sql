/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  reynoldscomputer
 * Created: 16 nov. 2022
    
    table test with Postgresql

 */

create role framework login password 'framework';
create database frameworkdb;
alter database frameworkdb owner to framework;

create table Person(
    idPerson serial primary key,
    nom varchar(100),
    prenom varchar(100),
    age int default 1
);

create table Dept(
    idDept serial primary key,
    nomDept varchar(100)
);

insert into dept(nomDept) values ('Informatique');
insert into dept(nomDept) values ('Commerce');
insert into dept(nomDept) values ('Multimedia');

