/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utilitaire;

/**
 *
 * @author reynoldscomputer
 */
public class MyUrlNotSupportedException extends Exception {
    private String message="UrlNotSupportedException : My Exception";

    public String getMessage() {
        return message;
    }

    public MyUrlNotSupportedException() {
    }
    
}
