/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utilitaire;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author reynoldscomputer
 */
public class Utilitaire {

    // sprint3
    public Field[] getField(Object o) {
        Field[] field = o.getClass().getDeclaredFields();
        return field;
    }

    public String myCapitalize(String string) {
        String result = string.substring(0, 1).toUpperCase() + string.substring(1);
        return result;
    }

    // sprint2-feature1
    public HashMap<String, String> getClassMethod() throws Exception {
        HashMap<String, String> result = new HashMap<String, String>();

        try {
            List<Class> classes = this.getAllClassPackage("model");
            MyUrlAnnotation urlAnnotation = null;
            for (int i = 0; i < classes.size(); i++) {
                Class c = classes.get(i);
                if (c.isAnnotationPresent(MyUrlAnnotation.class)) {
                    urlAnnotation = (MyUrlAnnotation) c.getAnnotation(MyUrlAnnotation.class);
                    String classAnnotation = urlAnnotation.Url();
                    //System.out.println("ClassAnnotation = "+classAnnotation);

                    result.put(classAnnotation, c.getSimpleName());

                    Method[] methods = this.getMethods(c);

                    for (Method method : methods) {
                        if (method.isAnnotationPresent(MyUrlAnnotation.class)) {

                            urlAnnotation = (MyUrlAnnotation) method.getAnnotation(MyUrlAnnotation.class);
                            String methodAnnotation = urlAnnotation.Url();

                            String key = classAnnotation + "-" + methodAnnotation;

                            result.put(key, method.getName());

                        }
                    }
                }

            }
        } catch (Exception ex) {

            throw ex;
        }

        return result;
    }

    public Method[] getMethods(Class c) throws Exception {
        Method[] result = null;

        try {
            result = c.getDeclaredMethods();
        } catch (Exception ex) {
            throw ex;
        }

        return result;
    }

    public List<Class> getAllClassPackage(String packageName) throws ClassNotFoundException {
        List<Class> result = null;
        result = new ArrayList<Class>();

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        String path = packageName.replace('.', '/');
        URL ressources = classLoader.getResource(path);

        File directory = new File(ressources.getFile());
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.getName().endsWith(".class")) {
                String[] java = file.getName().split("\\.");
                String className = java[0];

                try {
                    Class c = Class.forName(packageName + "." + className);
                    result.add(c);

                } catch (ClassNotFoundException ex) {
                    throw ex;
                }
            }
        }

        return result;
    }

    
    //sprint1-feature2
    public String retrieveUrlFromRawurl(String url) {
        String result = "";

        String[] tab = url.split("/");

        String[] tmp = tab[2].split("\\.");

        result = tmp[0];

        return result;
    }

    public Utilitaire() {
    }

}
