/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.PersonDAO;
import java.util.HashMap;
import utilitaire.MyUrlAnnotation;
import viewModel.ModelView;

/**
 *
 * @author reynoldscomputer
 */
@MyUrlAnnotation(Url = "personne")

public class Person {

    private String nom = "";
    private String prenom = "";
    private int age = 1;

    @MyUrlAnnotation(Url = "list")
    public ModelView list() throws Exception {

        ModelView mv = new ModelView();
        mv.setPage("pages/listPerson.jsp");

        PersonDAO dao = new PersonDAO();
        HashMap<String, Object> hashMap = new HashMap<String, Object>();

        try {
            System.out.println("List!");
            Object[][] select = dao.select(new Person());
            hashMap.put("listPerson", select);
        } catch (Exception ex) {
            throw ex;
        }

        mv.setData(hashMap);

        return mv;
    }

    //sprint2-feature3
    @MyUrlAnnotation(Url = "insert")
    public ModelView insert(Object o) throws Exception {
        System.out.println("Person insert here");
        ModelView mv = new ModelView();
        
        mv.setPage("personne-list.do");
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("olona", "Reynolds");

        PersonDAO dao = new PersonDAO();

        try {
            dao.insert(o);
        } catch (Exception ex) {
            throw ex;
        }

        mv.setData(hashMap);

        return mv;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Person() {
    }

}
