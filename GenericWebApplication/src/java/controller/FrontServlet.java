/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utilitaire.MyUrlNotSupportedException;
import utilitaire.Utilitaire;
import viewModel.ModelView;

/**
 *
 * @author reynoldscomputer
 */
public class FrontServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void init() throws ServletException {
        Utilitaire utilitaire = new Utilitaire();
        ServletContext context = this.getServletContext();

        try {
            context.setAttribute("list", utilitaire.getClassMethod());
        } catch (Exception ex) {
            System.out.println("init Exception : " + ex);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FrontServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FrontServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");

            Utilitaire utilitaire = new Utilitaire();
            ServletContext context = this.getServletContext();

            //sprint1-feature2
            String url = utilitaire.retrieveUrlFromRawurl(request.getRequestURI());

            String[] tab = url.split("-");
            String className = tab[0];
            String method = tab[1];

            String myUrl = className + "-" + method;

            try {

                HashMap<String, String> contextHashMap = (HashMap<String, String>) context.getAttribute("list");

                //sprint2-feature1
                if (contextHashMap.containsKey(myUrl)) {
                    out.println("myUrl exists! " + myUrl + " <br/>");

                    //sprint2-feature3
                    Class c = Class.forName("model." + contextHashMap.get(className));
                    Object o = c.newInstance();
                    Method classMethod = null, urlMethod = null;
                    
                    if (method.equals("insert")) {
                        classMethod = c.getMethod(method, Class.forName("java.lang.Object"));
                    }
                    else{
                        classMethod = c.getMethod(method);
                    }
                    
                    urlMethod = classMethod;

                    //sprint3-feature1
                    if (contextHashMap.containsKey(className)) {

                        Field[] fields = utilitaire.getField(o);

                        for (Field field : fields) {

                            if (request.getParameter(field.getName()) != null) {
                                out.println("fieldName = " + field.getName() + ", type = " + field.getType().getSimpleName() + "<br/>");
                                Object value = null;

                                String attribut = utilitaire.myCapitalize(field.getName());
                                String setter = "set" + attribut;
                                classMethod = null;

                                if (field.getType().getSimpleName().equals("int") || field.getType().getSimpleName().equals("Integer")) {
                                    value = Integer.parseInt(request.getParameter(field.getName()));
                                    classMethod = c.getMethod(setter, Class.forName("java.lang.Integer"));
                                } else {
                                    value = Class.forName(field.getType().getName()).cast(request.getParameter(field.getName()));
                                    classMethod = c.getMethod(setter, Class.forName(field.getType().getName()));
                                }

                                classMethod.invoke(o, value);

                                out.println("value = " + value + ", method = " + classMethod + "<br/>");
                                String getter = "get" + attribut;
                                classMethod = c.getMethod(getter);
                                out.println(">>get method = " + classMethod.invoke(o) + "<br/>");

                            }

                        }

                        ModelView mv = null;
                        
                        if (method.equals("insert")) {
                            System.out.println("INSERT");
                            urlMethod = c.getMethod(method, Class.forName("java.lang.Object"));
                            mv = (ModelView) urlMethod.invoke(o, o);
                        }
                        else{
                            System.out.println("TSY INSERT");
                            mv = (ModelView) urlMethod.invoke(o);
                        }
                        
                        
                        out.println(mv.getPage() + "<br/>");
                        out.println(mv.getHashMap() + "<br/>");

                        if (mv.getHashMap() != null) {
                            for (String key : mv.getHashMap().keySet()) {
                                Object value = mv.getHashMap().get(key);
                                out.println(key + " = " + value + "<br/>");
                                request.setAttribute(key, value);

                            }
                        }

                        RequestDispatcher dispat = request.getRequestDispatcher(mv.getPage());
                        dispat.forward(request, response);

                    }

                } else {
                    // sprint2-feature2
                    throw new MyUrlNotSupportedException();
                }

            } catch (Exception ex) {
                out.println("Exception : " + ex);
                ex.printStackTrace();
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
