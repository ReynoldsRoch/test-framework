/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package viewModel;

import java.util.HashMap;

/**
 *
 * @author reynoldscomputer
 */
public class ModelView {

    private HashMap<String, Object> hashMap = new HashMap();
    private String page;

    public void setData(HashMap<String, Object> data){
        this.hashMap = data;
    }
    
    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public HashMap<String, Object> getHashMap() {
        return hashMap;
    }

    public ModelView() {
    }

}
