/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.DeptDAO;
import java.util.HashMap;
import utilitaire.MyUrlAnnotation;
import viewModel.ModelView;

/**
 *
 * @author reynoldscomputer
 */
@MyUrlAnnotation(Url = "departement")

public class Dept {

    private String nomDept;

    //sprint2-feature3
    @MyUrlAnnotation(Url = "list")

    public ModelView list() throws Exception {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("dept", "Tana");
        ModelView mv = new ModelView();
        mv.setPage("pages/listDept.jsp");
        
        DeptDAO dao = new DeptDAO();
        
        try {
            hashMap.put("listDept", dao.select(new Dept()));
        } catch (Exception ex) {
            throw ex;
        }
        
        mv.setData(hashMap);
        return mv;
    }

    public String getNomDept() {
        return nomDept;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }

    public Dept() {
    }

}
