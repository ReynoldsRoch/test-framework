/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author reynoldscomputer
 */
public class GenericDAO {

    // select
    public Object[][] select(Object o, Connection con) throws Exception {

        String tab = o.getClass().getSimpleName();
        int nbLine = this.countNbLine(o, con), nbAttribut = this.nbAttribut(o);

        Object[][] result = new Object[nbLine][nbAttribut + 1];

        Statement stmt = null;
        ResultSet rs = null;

        if (o.getClass().isAnnotationPresent(TableName.class)) {
            TableName tabName = (TableName) o.getClass().getAnnotation(TableName.class);
            tab = tabName.nomTable();
        }

        String request = "select * from " + tab;

        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(request);
            int row = 0;
            while (rs.next()) {
                //System.out.println("\trow="+row);
                for (int col = 1, j = 0; col <= nbAttribut + 1; col++, j++) {

                    //System.out.println(rs.getString(col));
                    String test = (rs.getString(col));

                    result[row][j] = test;

                    //System.out.println("test index:"+col+"="+test);
                    //System.out.println("result with row=" + row + " and j=" + j + " , result=" + result[row][j]);
                }
                row++;
            }
            
        } catch (Exception e) {
            System.out.println("Exception Select : "+e);
            throw e;
        } finally {
            stmt.close();
            rs.close();
        }
        return result;
    }

    public int countNbLine(Object o, Connection con) throws Exception {
        String tab = o.getClass().getSimpleName();

        int result = 0;

        Statement stmt = null;

        ResultSet rs = null;

        String request = "select count(*) from " + tab;

        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(request);
            while (rs.next()) {
                result = Integer.parseInt(rs.getString(1));

            }
        } catch (Exception e) {
            result = 0;
            System.out.println("probleme : " + e);
        } finally {
            rs.close();
            stmt.close();
        }
        return result;
    }

    public List<List<Object>> selectList(Object o, Connection con) throws Exception {
        List<List<Object>> result = null;
        result = new ArrayList<List<Object>>();

        Class c = o.getClass();

        String tab = c.getSimpleName();

        if (c.isAnnotationPresent(TableName.class)) {
            TableName tabName = (TableName) c.getAnnotation(TableName.class);
            tab = tabName.nomTable();
        }

        String sql = "select * from " + tab;

        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);

            int row = 0;
            int nbAttribut = this.nbAttribut(o);

            while (rs.next()) {
                List<Object> object = new ArrayList<>();
                for (int col = 1, j = 0; col <= nbAttribut + 1; col++, j++) {

                    //System.out.println(rs.getString(col));
                    String test = (rs.getString(col));

                    object.add(test);

                    //System.out.println("object row : " + row + " element j : " + j + " = " + object.get(j));
                }
                result.add(object);
                row++;
            }

        } catch (Exception ex) {
            System.out.println(ex);
            throw ex;
        } finally {
            rs.close();
            stmt.close();
        }

        return result;
    }

    // create table
    public void createTable(Object o, Connection con) throws Exception {
        String tab = "";

        Class c = o.getClass();

        // tableName
        tab = c.getSimpleName();

        if (c.isAnnotationPresent(TableName.class)) {
            TableName tabName = (TableName) c.getAnnotation(TableName.class);
            tab = tabName.nomTable();
        }

        String sql = "create table " + tab + "(";

        //id
        sql += "id serial primary not null,";

        // column
        Field[] field = this.getField(o);
        for (int i = 0; i < field.length; i++) {
            Field field1 = field[i];
            String typeColumn = "";
            String col = field1.getName();
            if (field1.isAnnotationPresent(MyAnnotation.class)) {
                col = field1.getAnnotation(MyAnnotation.class).nom();
            }

            typeColumn = field1.getType().getSimpleName();

            if (field1.getType().getSimpleName().equals("String")) {
                typeColumn = "varchar(100)";
            }

            if (field1.getType().getSimpleName().equals("double")) {
                typeColumn += " precision";
            }

            if (i == field.length - 1) {
                sql += col + " " + typeColumn;
            } else {
                sql += col + " " + typeColumn + ", ";
            }
        }

        sql += ");";

        System.out.println("sql = " + sql);

        Statement stmt = null;

        try {
            stmt = con.createStatement();
            stmt.execute(sql);

            stmt.close();
        } catch (Exception ex) {
            System.out.println(ex);
            throw ex;
        }

    }

    // insert
    public void insert(Object o, Connection con) throws Exception {

        String sql = "";

        Statement stmt = null;

        try {
            sql = this.generateInsertSql(o);

            System.out.println(sql);
            
            stmt = con.createStatement();
            stmt.execute(sql);
            
        } catch (Exception ex) {
            throw ex;
        } finally {
            stmt.close();
        }

    }

    public String generateInsertSql(Object o) throws Exception {
        
        String result = "";

        String tab = "";

        Class c = o.getClass();

        // table
        tab = c.getSimpleName();

        if (c.isAnnotationPresent(TableName.class)) {
            TableName tabName = (TableName) c.getAnnotation(TableName.class);
            tab = tabName.nomTable();
        }

        String sql = "insert into " + tab + "(";

        // column
        Field[] field = this.getField(o);
        for (int i = 0; i < field.length; i++) {
            Field field1 = field[i];
            String col = field1.getName();
            if (field1.isAnnotationPresent(MyAnnotation.class)) {
                col = field1.getAnnotation(MyAnnotation.class).nom();
            }

            if (i == field.length - 1) {
                sql += col;
            } else {
                sql += col + ",";
            }
        }

        sql += ") values (";

        //values
        for (int i = 0; i < field.length; i++) {
            Field field1 = field[i];
            String attribut = "get" + this.myCapitalize(field1.getName());
            Method method;
            try {
                method = o.getClass().getMethod(attribut);

                if (i == this.nbAttribut(o) - 1) {
                    sql += "'" + method.invoke(o) + "'";
                } else {
                    sql += "'" + method.invoke(o) + "',";
                }
            } catch (Exception ex) {
                throw ex;
            }

        }

        sql += ")";

        // sql
        result = sql;

        return result;
    }

    public String myCapitalize(String string) {
        String result = string.substring(0, 1).toUpperCase() + string.substring(1);
        return result;
    }

    // field
    public String[] getFieldName(Object o) {
        String[] result = new String[this.nbAttribut(o)];
        Field[] field = this.getField(o);

        for (int i = 0; i < result.length; i++) {
            String string = field[i].getName();
            result[i] = string;
        }

        return result;
    }

    public int nbAttribut(Object o) {
        int result = 0;
        result = this.getField(o).length;
        return result;
    }

    public Field[] getField(Object o) {
        Field[] field = o.getClass().getDeclaredFields();
        return field;
    }

    public GenericDAO() {
    }

}
