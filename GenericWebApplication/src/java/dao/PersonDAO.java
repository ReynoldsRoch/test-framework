/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import utilitaire.MyUrlAnnotation;

/**
 *
 * @author reynoldscomputer
 */
public class PersonDAO {
    
    private ConnectionDAO con = new ConnectionDAO("framework", "frameworkdb", "framework");
    private GenericDAO dao = new GenericDAO();
    
    public void insert(Object o) throws Exception{
        Connection connection = null;
        try {
            connection = con.connectPostgres();
            
            dao.insert(o, connection);
            
            connection.close();
        } catch (Exception ex) {
            throw ex;
        }

    }
    
    @MyUrlAnnotation(Url = "listPerson" )
    public Object[][] select(Object o) throws Exception{
        Object[][] result = null;
        
        Connection connection = null;
        try {
            connection = con.connectPostgres();
            
            result = dao.select(o, connection);
            
        } catch (Exception ex) {
            System.out.println("Exception Person Select : "+ex);
            throw ex;
        } finally {
            connection.close();
        }
        
        return result;
    }

    public PersonDAO() {
    }
    
}
