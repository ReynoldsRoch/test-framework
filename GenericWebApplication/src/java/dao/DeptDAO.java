/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;

/**
 *
 * @author reynoldscomputer
 */
public class DeptDAO {
    
    private ConnectionDAO con = new ConnectionDAO("framework", "frameworkdb", "framework");
    private GenericDAO dao = new GenericDAO();
    
    public Object[][] select(Object o) throws Exception{
        Object[][] result = null;
        
        Connection connection = null;
        
        try {
            connection = con.connectPostgres();
            result = dao.select(o, connection);
        } catch (Exception ex) {
            throw ex;
        } finally {
            connection.close();
        }
        
        return result;
    }

    public DeptDAO() {
    }
    
}
