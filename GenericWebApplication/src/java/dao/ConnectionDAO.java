/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author reynoldscomputer
 */
public class ConnectionDAO {

    private String user;
    private String db;
    private String pass;

    public Connection connectPostgres() throws Exception {
        Connection con = null;

        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + this.db, this.user, this.pass);

        } catch (Exception e) {
            System.out.println("Exception : " + e);
            throw e;       
        }

        return con;
    }

    public ConnectionDAO(String user, String db, String pass) {
        this.user = user;
        this.db = db;
        this.pass = pass;
    }

}
