/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package app;

import java.lang.reflect.Method;
import model.Person;
import utilitaire.Utilitaire;
import viewModel.ModelView;

/**
 *
 * @author reynoldscomputer
 */
public class App {

    public static void main(String[] args) throws Exception {
        ModelView modelView = new ModelView();
        Utilitaire util = new Utilitaire();

        try {

            //System.out.println(util.getClassMethod());
            Class c = Class.forName("model.Person");
            String method = "insert";
            Object o = c.newInstance();
            Method classMethod = null;
            classMethod = c.getMethod(method, Class.forName("java.lang.Object"));
            
            Person person = new Person();
            person.setAge(20);
            person.setNom("Roch");
            person.setPrenom("Reynolds");
            
            ModelView mv = (ModelView) classMethod.invoke(o, person);
            System.out.println(mv.getPage() + "<br/>");
            System.out.println(mv.getHashMap() + "<br/>");

//        System.out.println(modelView.getHashMap());
        } catch (Exception ex) {
            throw ex;
        }

    }

}
